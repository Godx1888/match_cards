﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace matchCards
{
    class Card
    {
        // ---------------------------------------
        // Data
        //----------------------------------------

        Texture2D image;
        Vector2 position = Vector2.Zero;

        // ---------------------------------------
        // Behaviour
        // ---------------------------------------
        public void LoadContent(ContentManager content)
        {

            image = content.Load<Texture2D>("graphics/bear");


            switch (species)
            {
                case Species.Bear:
                    image = content.Load<Texture2D>("graphics/bear");
                    critterValue = 20;
                    break;

                case Species.Elephant:
                    image = content.Load<Texture2D>("graphics/elephant");
                    critterValue = 30;
                    break;

                case Species.Horse:
                    image = content.Load<Texture2D>("graphics/horse");
                    critterValue = 50;
                    break;

                default:
                    break;
            }




            clickCritter = content.Load<SoundEffect>("audio/buttonclick");

        }
    }
}
